default: ticktock
	g++ main.cpp reader.cpp jsonparser.cpp result.cpp db.cpp ticktock-master/ticktock.o  -pthread  -o log_reader  -g

ticktock: 
	cd ticktock-master; make; cd ..

generate_tests: 
	cd test_examples; ruby generate_examples.rb; cd ..

clean: 
	\rm -f *.o; cd ticktock-master; make clean; cd ..

run: 
	./log_reader --nthreads 4

