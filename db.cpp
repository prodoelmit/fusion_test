#include "db.h"

DB::DB(): m_counter(0) {

    m_tt.tick();
}

DB* DB::operator<<(Result* r) {

    if (m_counter % 100000 == 0) {
        
        double t = m_tt.silent_tock();
        m_tt.tick();
        if (m_counter > 0) {
            cout << "Speed = " << 100000. / t << "rec/s" << endl;
        }

    }
    m_counters[r->m_date][r->m_name][r->m_props]++;

    m_counter++;
    return this;
}

void DB::printSummary() {
    cout << m_counters.size() << endl << flush;
}

void DB::dump(string filename) {
    cout << "DB had " << m_counter << "imports" << endl;
    json j(m_counters);


    ofstream o(filename);

    o << setw(10) << j << endl;

    o.close();


}
