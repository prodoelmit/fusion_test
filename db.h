#ifndef DB_H
#define DB_H
#include "result.h"
#include <unordered_map>
#include <ctime>
#include <functional>
#include "ticktock-master/ticktock.h"
#include <fstream>
using namespace std;


class DB {
    public:
    DB();

    DB* operator<<(Result* r);

    void printSummary();
    void dump(string filename);


    private:
        
        TickTock m_tt;
        time_t m_tstart, m_tend;
        unordered_map<Date, unordered_map<string, unordered_map<PropsList, int> > > m_counters;
        int m_counter;
    

};
#endif
