#include "jsonparser.h"
#include <unistd.h>
#include <iostream>
#include "json.hpp"
using namespace std;
using json = nlohmann::json;

JSONParser::JSONParser()
    : m_finished(false)
      , m_aboutToFinish(false)
      , m_waitingToParse(false)
      , m_results(0)
      , m_parsedCounter(0)
{

    m_parseMutex = new mutex;
    m_resultMutex = new mutex;
    m_parseBuffer = "hi";

    m_thread = new thread(&JSONParser::runWorker, this);
}

JSONParser::~JSONParser() {

}


void JSONParser::runWorker() {
    while (!m_aboutToFinish) {
        //lock mutex
        m_parseMutex->lock();
        if (!m_waitingToParse) {
            m_parseMutex->unlock();
            continue;
        }


        // parse 
        auto j = json::parse(m_parseBuffer);
        Result* r;
        r = new Result(j);

        // set result
        m_resultMutex->lock();
        m_results++;
        m_resultsBuffer.push(r);
        m_parsedCounter++;
        m_resultMutex->unlock();



        m_waitingToParse = false;


        //unlock mutex
        m_parseMutex->unlock();
    }
}

bool JSONParser::readyToParse(){
    if (m_parseMutex->try_lock()) {
        m_parseMutex->unlock();
        return true;
    } else {
        return false;
    }
}

bool JSONParser::tryToParse(string s) {
    if (!readyToParse() || m_waitingToParse) {
        return false;
    } else {
        m_parseMutex->lock();
        m_parseBuffer = s;
        m_waitingToParse = true;
        m_parseMutex->unlock();
        return true;
    }
}

void JSONParser::parse(string s){
    if (!readyToParse()) {
        return;
    }
    m_parseBuffer = s;
    m_waitingToParse = true;
}

bool JSONParser::hasResult(){
    m_resultMutex->lock();
    bool has = m_resultsBuffer.size() > 0;
    m_resultMutex->unlock();
    return has;
}

Result* JSONParser::popResult(){
    m_resultMutex->lock();
    Result* r = m_resultsBuffer.front();
    m_resultsBuffer.pop();
    m_resultMutex->unlock();
    return r;
}

void JSONParser::setID(int id) {
    m_id = id;
}

void JSONParser::waitForFinished() {
    while (m_waitingToParse) {

    }
    m_parseMutex->lock();
    m_aboutToFinish = true;
    m_parseMutex->unlock();
    m_thread->join();
    cout << "JSONParser parsed " << m_parsedCounter << endl;
}

