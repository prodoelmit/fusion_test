#ifndef JSONPARSER_H
#define JSONPARSER_H
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include <queue>
#include "result.h"
using namespace std;


class JSONParser {
    public: 
    JSONParser();
    ~JSONParser();

    void runWorker();
    bool readyToParse();
    void parse(string s);
    bool hasResult();
    Result* popResult();

    void waitForFinished();
    void setID(int id);
    bool tryToParse(string s);

    private:


    mutex* m_parseMutex;
    mutex* m_resultMutex;
    string m_parseBuffer;
    queue<Result*> m_resultsBuffer;


    thread* m_thread;

    bool m_waitingToParse;
    int m_results;


    
    int m_id;

    bool m_finished;
    bool m_aboutToFinish;

    int m_parsedCounter;
    


};


#endif
