#include <iostream>
#include "reader.h"
#include "db.h"
#include <unistd.h>
using namespace std;


int main(int argc, char** argv) {

    int nthreads = 7;
    int nfiles = 4;

    string dirPath = "test_examples";
    string outPath = "output/agr.txt";

    for (int i = 0; i < argc; i++) {
        string s = argv[i];
        if (s == "--nthreads") {
            i++;
            if (i < argc) {
                nthreads = atoi(argv[i]);
            }
        } else if (s == "--nfiles") {
            i++;
            if (i < argc) {
                nfiles = atoi(argv[i]);
            }
        } else if (s == "--dir") {
            i++;
            if (i < argc) {
                dirPath = argv[i];
            }
        } else if (s == "--outFile") {
            i++;
            if (i < argc) {
                outPath = argv[i];
            }
        } else if (s == "-h") {
            cout << "Usage: ./log_reader [--nthreads n] [--nfiles n] [--dir d] [--outFile f]" << endl;

            return 0;
        }
    }

    DB* db = new DB();
    Reader* reader = new Reader(nthreads, nfiles, dirPath, db);

    reader->readAll();

    db->dump(outPath);

    return 0;
}


