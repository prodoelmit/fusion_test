#include "reader.h"
#include <iostream>
#include <fstream>
#include <unistd.h>
using namespace std;

Reader::Reader(int nthreads, int nfiles, string dirPath, DB* db):
    m_nthreads(nthreads)
    , m_nfiles(nfiles)
    , m_dirPath(dirPath) 
    , m_db(db)
    , m_aboutToFinish(false){

        if (m_nthreads < 3) {
            m_nthreads = 3;
        }

        initParsers();
        initReducer();

}


void Reader::initParsers() {
    for (int i = 2; i < m_nthreads; i++) {
        JSONParser* p = new JSONParser();
        p->setID(i);
        m_parsers.push_back(p);
    }
}

void Reader::readFile(string filename) {
    cout << filename << endl;
    ifstream in(filename);
    string s;
    char c;

    int cBraceCounter = 0;
    bool atLeastOneBraceFound = false;

    if (in.is_open()) {
        while (in.good()) {
            in.get(c);
            if (c == '{') {
                atLeastOneBraceFound = true;
                cBraceCounter++;
            } else if (c == '}') {
                cBraceCounter--;
                atLeastOneBraceFound = true;
            }
            if (atLeastOneBraceFound) {
                s += c;
            }
            if (atLeastOneBraceFound && cBraceCounter == 0) {
                enqueueString(s);
                s = "";
                atLeastOneBraceFound = false;
            } 
            if (cBraceCounter < 0) {
                cerr << "Mismatching braces";
            }
        }
    }
    if (!in.eof() && in.fail()) {
        cerr << "Error reading file" <<  filename;
    }

    in.close();
}


void Reader::reduceResults() {
    int i = 0;
    int j = 0;
    bool lastRun = false;
    while (true) {
        if (i == 0 && m_aboutToFinish) {
            lastRun = true;
            cout << j << " parsed when called finalize()" << endl;
        }

        JSONParser* p = m_parsers[i];
        while (p->hasResult()) {
            Result* r = p->popResult();
            *m_db << r;
            j++;
        }
        i = (i+1) % m_parsers.size();

        if (i == 0 && lastRun) {
            break;
        }
    }

}

void Reader::enqueueString(string s) {
    int i = 0;
    while (true) {
        JSONParser* p = m_parsers[i];
        if (p->tryToParse(s)) {
            break;
        }
        i = (i+1) % m_parsers.size();
    }
}

void Reader::initReducer() {
    m_reducerThread = new thread(&Reader::reduceResults, this);
}

void Reader::readAll() { 
    for (int i = 1; i <= m_nfiles; i++) {
        string filename = m_dirPath + "/" + string("file")+to_string(i)+".log";
        readFile(filename);
    }
    for (int i = 0; i < m_parsers.size(); i++) {
        m_parsers[i]->waitForFinished();
    }
    finalize();
}

void Reader::finalize() {
    m_aboutToFinish = true;
    m_reducerThread->join();
}
