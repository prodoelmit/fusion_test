#ifndef READER_H
#define READER_H
#include <thread>
#include <iostream>
#include <vector>
#include <mutex>
    //#include "resultqueue.h"
#include "jsonparser.h"
#include "reader.h"
#include "db.h"
    using namespace std;

    class Reader {

        public: 
            Reader(int nthreads, int nfiles, string dirPath, DB* db);

            void readAll();
            void enqueueString(string s);
            void reduceResults();

        private:


            void readFile(string filename);

        void initParsers();
        void initReducer();

        void finalize();

        vector<JSONParser*> m_parsers;

     	int m_nthreads;
     	int m_nfiles;
     	string m_dirPath;
        DB* m_db;
        thread* m_reducerThread;
        bool m_aboutToFinish;
        



};

#endif
