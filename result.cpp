#include "result.h"
using namespace std;

Result::Result(json& j) {

    auto ts_fact = j["ts_fact"];
    auto t = time_t(j["ts_fact"].get<int>());
    auto gm = gmtime(&t);
    m_date = Date( gm->tm_year, gm->tm_mon, gm->tm_mday );

    m_name = j["fact_name"];
    json props = j["props"];

    m_props.push_back(props["prop1"] .get<int>());
    m_props.push_back(props["prop2"] .get<int>());
    m_props.push_back(props["prop3"] .get<int>());
    m_props.push_back(props["prop4"] .get<int>());
    m_props.push_back(props["prop5"] .get<int>());
    m_props.push_back(props["prop6"] .get<int>());
    m_props.push_back(props["prop7"] .get<int>());
    m_props.push_back(props["prop8"] .get<int>());
    m_props.push_back(props["prop9"] .get<int>());
    m_props.push_back(props["prop10"].get<int>());
    m_props.computeHash();
    

}
