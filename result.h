#ifndef RESULT_H
#define RESULT_H
#include "json.hpp"
#include <ctime>
#include <string>


using json = nlohmann::json;
using namespace std;

class Date {
    public:

    Date(int y = 0, int m = 0, int d = 0): 
        m_year(y), m_month(m), m_day(d) {}
    bool operator==(const Date d) const {
        return m_year == d.m_year && m_month == d.m_month && m_day == d.m_day;
    }
    int m_year;
    int m_month;
    int m_day;

    operator string() const {
        return std::string("") + to_string(m_year + 1900) + "-" + to_string(m_month+1) + "-" + to_string(m_day);
    }

};

class PropsList {
    public :

        PropsList() {
        };
        bool operator==(const PropsList pl) const {
            return hash() == pl.hash();
        }
        void push_back(int p) {
            m_props.push_back(p);
        }
        void computeHash() {
            string s;
            for (int i = 0; i < m_props.size(); i++) {
                if (i) {
                    s += ",";
                }
                s += to_string(m_props[i]);
            }
            m_hash = std::hash<std::string>()(s);
        }
        size_t hash() const {
            return m_hash;
        }
        operator std::string() const {
            string s;
            for (int i = 0; i < m_props.size(); i++) {
                if (i) {
                    s += ",";
                }
                s += to_string(m_props[i]);
            }
            return s;
        }

    private:
        vector<int> m_props;
        size_t m_hash;

};

namespace std {
    template <>
        struct hash<Date>
        {
            size_t operator()(const Date& d) const
            {
                return d.m_year * 31*12 + d.m_month * 31 + d.m_day;
            }
        };

    template <>
        struct hash<PropsList>
        {
            size_t operator()(const PropsList& pl) const
            {
                return pl.hash();
            }
        };

}


class Result {

    public:
        Result(json& j);

        Date m_date;
        string m_name;
        PropsList m_props;




};

#endif
