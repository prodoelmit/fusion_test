require 'json'
require 'time'

def random_obj 
    {ts_fact: (Time.now).to_i,
     fact_name: "fact#{rand(10)}",
     actor_id: rand(100000),
     props: {
         prop1: rand(30),
         prop2: rand(30),
         prop3: rand(30),
         prop4: rand(30),
         prop5: rand(30),
         prop6: rand(30),
         prop7: rand(30),
         prop8: rand(30),
         prop9: rand(30),
         prop10: rand(30)
     }
    }
end

n = 100000

(1..5).each do |j|
    open("file#{j}.log", 'w') do |f|
        n.times do |i|
            f.puts random_obj.to_json
        end
    end
end
     

